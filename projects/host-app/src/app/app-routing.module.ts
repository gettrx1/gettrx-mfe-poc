import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'mfe',
    loadChildren: () => import('mfe-app/HomeModule').then((m) => m.HomeModule),
  },
  {
    path: '',
    redirectTo: 'mfe',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
